package be.integrationdesigners.hikeman;

import org.apache.camel.spi.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.transaction.Transactional;

import java.util.List;

import static java.util.Objects.isNull;

@Named("rankingService")
@ApplicationScoped
public class RankingService {
    private static Logger LOG = LoggerFactory.getLogger(RankingService.class);

    @Transactional
    public void updateRanking(ItemDTO itemDTO) {
        LOG.info("updating ranking for " + itemDTO.userId);
        RankingEntity ranking = (RankingEntity) RankingEntity.find("userId", itemDTO.userId).firstResultOptional().orElse(null);

        if (isNull(ranking)) {
            LOG.info("Ranking not found for user. Creating new one");
            ranking = new RankingEntity();
            ranking.userId = itemDTO.userId;
            ranking.positionDay = 1;
            ranking.positionWeek = 1;
            ranking.positionMonth = 1;
            ranking.positionYear = 1;
            ranking.persist();
        }
        else {
            ranking.positionDay++;
            ranking.positionWeek++;
            ranking.positionMonth++;
            ranking.positionYear++;
        }

    }
}

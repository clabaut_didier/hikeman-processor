package be.integrationdesigners.hikeman;

import org.apache.camel.BeanInject;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.elasticsearch.action.get.MultiGetRequest;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class HikemanEventListener extends RouteBuilder {

    @ConfigProperty(name="kafka.external.bootstrap.url")
    String bootstrapServerUrl;

    @BeanInject("rankingService")
    RankingService rankingService;

    @Override
    public void configure() throws Exception {
        from("kafka:hikeman?brokers=" + bootstrapServerUrl)
                .log("Message received from Kafka : ${body}")
                .unmarshal().json(JsonLibrary.Jackson, ItemDTO.class)
                .log("Marshalled version : ${body}")
                .log("    on the topic ${headers[kafka.TOPIC]}")
                .to("bean:rankingService?method=updateRanking(${body})");
    }
}

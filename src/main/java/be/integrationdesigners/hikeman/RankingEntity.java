package be.integrationdesigners.hikeman;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "ranking")
@ToString
public class RankingEntity extends PanacheEntityBase {
    @Id
    @Column(name = "user_id")
    @Type(type="uuid-char")
    public UUID userId;
    @Column(name = "position_day")
    public int positionDay;
    @Column(name = "position_week")
    public int positionWeek;
    @Column(name = "position_month")
    public int positionMonth;
    @Column(name = "position_year")
    public int positionYear;
}
//{"userId":"41be6849-5710-11eb-ace5-f6f3175b901d","itemId":"9b2b90be-121b-47da-b824-1c170d838a66","longitude":"50.123457899","latitude":"51.414141414"}

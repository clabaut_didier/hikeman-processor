package be.integrationdesigners.hikeman;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.UUID;

public class ItemDTO {
    @JsonProperty("itemId")
    UUID itemId;
    @JsonProperty("userId")
    UUID userId;
    @JsonProperty("latitude")
    BigDecimal longitude;
    @JsonProperty("longitude")
    BigDecimal latitude;
}
